/*
 * Copyright 2018 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tdapp.paytonaidan.com.tdchallengeapp;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.database.Cursor;
import android.gesture.Gesture;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Transformation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.ar.core.Anchor;
import com.google.ar.core.HitResult;
import com.google.ar.sceneform.AnchorNode;
import com.google.ar.sceneform.Node;
import com.google.ar.sceneform.rendering.ModelRenderable;
import com.google.ar.sceneform.rendering.Renderable;
import com.google.ar.sceneform.rendering.ViewRenderable;
import com.google.ar.sceneform.ux.ArFragment;
import com.google.ar.sceneform.ux.DragGesture;
import com.google.ar.sceneform.ux.DragGestureRecognizer;
import com.google.ar.sceneform.ux.TransformableNode;
import com.google.ar.sceneform.ux.TransformationSystem;
import com.google.ar.sceneform.ux.TranslationController;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class MainActivity extends AppCompatActivity {
    private static final double MIN_OPENGL_VERSION = 3.0;
    private static final String TAG = MainActivity.class.getName();

    private ArFragment arFragment; //the ARFragment Reference

    private ViewRenderable loginRenderable; //login window
    private ViewRenderable accountRenderable; //account summary window
    private ModelRenderable walletRenderable; //wallet object
    private ViewRenderable savingsAccountRenderable; //account history renderable
    private ViewRenderable chequingAccountRenderable; //account history renderable

    private String chequingAmount; //holds the chequing amount balance
    private String savingAmount; //holds the savings account balance

    private String userName = "tdchallenge"; //holds the username from the dialog
    private String password = "password"; //holds the password from the dialog

    private AnchorNode globalAnchorNode; //a global anchor node used by the login and the wallet
    private TransformableNode globalTransformableNode; //a global Transformable Node used for login and wallet
    private Node globalNode;

    //Booleans
    private Boolean hasLoggedIn = false;

    public ArrayList<Transaction> transactions = new ArrayList<>();
    public ArrayList<String> savingsTransactions = new ArrayList<>();
    public ArrayList<String> chequingTransations = new ArrayList<>();

    //DB Adapter for user Database
    DBAdapterForUserDB db;
    DBAdapterForTransactionDB tranDB;

    //App Lifecycle Methods
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(!checkIsSupportedDevice(this)){
            String errorMessage = "Sceneform Requires OpenGL ES " + MIN_OPENGL_VERSION + " or later";
            Log.e(TAG, errorMessage);
            Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        setContentView(R.layout.activity_main);

        setupArScene();
        configDatabase();
        //createTransactions();
        //createUser();
    }
    @Override
    protected void onResume(){
        super.onResume();
    }

    //Database Methods
    public void configDatabase(){
        db = new DBAdapterForUserDB(this);
        tranDB = new DBAdapterForTransactionDB(this);
        try{
            String destPath = "data/data/" + getPackageName() + "/databases";
            File f = new File(destPath);
            if(!f.exists()){
                f.mkdirs();
                f.createNewFile();
                CopyUserDB(getBaseContext().getAssets().open("users"),
                        new FileOutputStream(destPath + "/users"));
            }
        }catch(FileNotFoundException ex){
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }
        catch(IOException e){
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }
    public void CopyUserDB(InputStream inputStream, OutputStream outputStream) throws IOException{
        byte [] buffer = new byte[1024];
        int length;
        while((length = inputStream.read(buffer)) > 0){
            outputStream.write(buffer,0,length);

        }
        inputStream.close();
        outputStream.close();

    }
    private void createUser(){
        try{
            db.open();
            db.insertUser("tdchallenge", "password", "254.56", "1867.45");
        }
        catch(Exception ex){

        }
    }
    private void createTransactions(){
        populateTransactions();
        try{
            tranDB.open();
            for(Transaction t : transactions){
                tranDB.insertTranscation(t.type, t.amount, t.account, t.username);
            }
        }
        catch(Exception ex){
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
            Log.d("ERROR", ex.getMessage());
        }

    }

    private void getTransactions(){
        try{
            tranDB.open();
            Cursor c;
            c = tranDB.getTransactionsByUsername(userName);
            if(c.moveToFirst()){
                do{
                    Transaction t = new Transaction(c.getString(1), c.getString(2), c.getString(3), c.getString(4));
                    if(t.account.equals("Savings")){
                        savingsTransactions.add(t.type + ": $" + t.amount);
                    }
                    else{
                        chequingTransations.add(t.type + ": $" + t.amount);
                    }
                }while(c.moveToNext());
            }
        }
        catch(Exception ex) {
            Log.e("ERROR", ex.getMessage());
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    //AR Methods
    private void setupArScene(){
        arFragment = (ArFragment)getSupportFragmentManager().findFragmentById(R.id.ux_fragment);
        arFragment.getArSceneView().getPlaneRenderer().setShadowReceiver(false);
        //Build the 3D Assets
        buildLogin();
        buildWallet();
        //buildAccountWindow();
        buildChequingActivity(); //builds the renderable that shows the user their account history
        buildSavingActivity();   //builds the renderable that shows the user their account history
        handleUserTaps();
    }
    private void handleUserTaps(){
        arFragment.setOnTapArPlaneListener(((hitResult, plane, motionEvent) -> {
            if (loginRenderable == null) {
                return;
            }
            if(!hasLoggedIn) { //if the user has not logged in then place a login renderable
                globalAnchorNode = createAnchorNode(hitResult);
                addRenderableToScene(globalAnchorNode, loginRenderable);
            }
            else{
                globalAnchorNode.setRenderable(null);
                globalAnchorNode = createAnchorNode(hitResult);
                addRenderableToScene(globalAnchorNode, accountRenderable);
            }

            })
        );
    }
    private boolean checkIsSupportedDevice(final Activity activity){
        ActivityManager activityManager = (ActivityManager)activity.getSystemService(Context.ACTIVITY_SERVICE);

        if(activityManager == null){
            Log.e(TAG, "ActivityManager is null");
            return false;
        }

        String openGLVersion = activityManager.getDeviceConfigurationInfo().getGlEsVersion();
        return openGLVersion != null && Double.parseDouble(openGLVersion) >= MIN_OPENGL_VERSION;

    }



    //Model Building Methods
    public void buildWallet(){
        ModelRenderable.builder()
                .setSource(this, Uri.parse("www.cadnav.com_2013-10-13_031.sfb"))
                .build()
                .thenAccept(renderable -> walletRenderable = renderable
                )
                .exceptionally(throwable -> {
                    Toast.makeText(MainActivity.this, "Unable to render", Toast.LENGTH_LONG).show();
                    return null;
                });
    }
    public void buildLogin(){
        ViewRenderable.builder()
                .setView(this, R.layout.button_view)
                .build()
                .thenAccept(renderable -> {
                    loginRenderable = renderable;
                    if(loginRenderable != null) {
                        View androidView = loginRenderable.getView();
                        Button usernameButton = androidView.findViewById(R.id.userNameButton);
                        Button passwordButton = androidView.findViewById(R.id.passwordButton);
                        Button confirmButton = androidView.findViewById(R.id.confirmButton);
                        Button closeButton = androidView.findViewById(R.id.cancelButton);
                        usernameButton.setOnClickListener(view -> {
                            UsernameDialog usernameDialog = new UsernameDialog();
                            usernameDialog.setOkListener(this::onUsernameOkPressed);
                            usernameDialog.show(getSupportFragmentManager(), "Username");
                        });
                        passwordButton.setOnClickListener(view ->{
                            PasswordDialog passwordDialog = new PasswordDialog();
                            passwordDialog.setOkListener(this::onPasswordOkPressed);
                            passwordDialog.show(getSupportFragmentManager(), "Password");
                        });
                        confirmButton.setOnClickListener(view ->{
                            onConfirmPressed();
                        });
                        closeButton.setOnClickListener(view ->{
                            cancelPressed();
                        });

                    }
                })
                .exceptionally(throwable -> {
                    Toast.makeText(MainActivity.this, "Unable to display model",
                            Toast.LENGTH_LONG).show();
                    return null;
                });
    }
    public void buildChequingActivity(){
        ViewRenderable.builder()
                .setView(this, R.layout.chequing_list_view)
                .build()
                .thenAccept(renderable ->{
                    chequingAccountRenderable = renderable;
                    if(chequingAccountRenderable != null){
                        View historyView = chequingAccountRenderable.getView();
                        Button backButton = historyView.findViewById(R.id.chequingBackButton);
                        ListView historyList = historyView.findViewById(R.id.chequingActivityView);

                        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, chequingTransations);
                        historyList.setAdapter(arrayAdapter);


                        backButton.setOnClickListener(view ->{
                            globalTransformableNode.setRenderable(null);
                            globalNode.setRenderable(null);
                            addRenderableToScene(globalAnchorNode, accountRenderable);
                        });
                    }
                })
                .exceptionally(throwable -> {
                    Toast.makeText(MainActivity.this, "Unable to display view", Toast.LENGTH_LONG).show();
                    return null;
                });
    }
    public void buildSavingActivity(){
        ViewRenderable.builder()
                .setView(this, R.layout.savings_list_view)
                .build()
                .thenAccept(renderable ->{
                    savingsAccountRenderable = renderable;
                    if(savingsAccountRenderable != null){
                        View historyView = savingsAccountRenderable.getView();
                        Button backButton = historyView.findViewById(R.id.savingsBackButton);
                        ListView historyList = historyView.findViewById(R.id.savingsActivityView);

                        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1, savingsTransactions);
                        historyList.setAdapter(arrayAdapter);

                        backButton.setOnClickListener(view ->{
                            globalTransformableNode.setRenderable(null);
                            globalNode.setRenderable(null);
                            addRenderableToScene(globalAnchorNode, accountRenderable);
                        });
                    }
                })
                .exceptionally(throwable -> {
                    Toast.makeText(MainActivity.this, "Unable to display view", Toast.LENGTH_LONG).show();
                    return null;
                });
    }
    public void buildAccountWindow(){
        ViewRenderable.builder()
                .setView(this, R.layout.account_view)
                .build()
                .thenAccept(renderable ->{
                    accountRenderable = renderable;
                    if(accountRenderable != null){
                        View accountView = accountRenderable.getView();
                        TextView cheqAmt = accountView.findViewById(R.id.cheqAmount);
                        TextView savAmount = accountView.findViewById(R.id.savAmount);
                        Button logoutButton = accountView.findViewById(R.id.AccountCancel);
                        Button confirmButton = accountView.findViewById(R.id.AccountConfirm);
                        ImageButton savingHistory = accountView.findViewById(R.id.savingListButton);
                        ImageButton chequingHistory = accountView.findViewById(R.id.chequingListButton);
                        cheqAmt.setText(chequingAmount);
                        savAmount.setText(savingAmount);
                        confirmButton.setOnClickListener(view ->{
                            globalTransformableNode.setRenderable(null);
                            //addRenderableToScene(globalAnchorNode, walletRenderable);
                        });
                        logoutButton.setOnClickListener(view ->{
                            globalTransformableNode.setRenderable(null);
                            hasLoggedIn = false;
                            Collection<Anchor> allAnchors = arFragment.getArSceneView().getSession().getAllAnchors();
                            for(Iterator<Anchor> iter = allAnchors.iterator(); iter.hasNext();){
                                Anchor anchor = iter.next();
                                anchor.detach();
                            }
                            password = null;
                            userName = null;
                            Toast.makeText(MainActivity.this, "Logged Out", Toast.LENGTH_LONG).show();
                        });
                        savingHistory.setOnClickListener(view ->{
                            globalTransformableNode.setRenderable(null);
                            addRenderableToSceneNoMove(globalAnchorNode, savingsAccountRenderable);
                        });
                        chequingHistory.setOnClickListener(view ->{
                            globalTransformableNode.setRenderable(null);
                            addRenderableToSceneNoMove(globalAnchorNode, chequingAccountRenderable);
                        });
                    }

                })
                .exceptionally(throwable -> {
                    Toast.makeText(MainActivity.this, "Unable to display model", Toast.LENGTH_LONG).show();
                    return null;
                });
    }


    //Creates a node for the anchor
    private AnchorNode createAnchorNode(HitResult hitResult){
        Anchor anchor = hitResult.createAnchor();
        AnchorNode anchorNode = new AnchorNode(anchor);
        anchorNode.setParent(arFragment.getArSceneView().getScene());
        return anchorNode;
    }

    private Node addRenderableToScene(AnchorNode anchorNode, Renderable renderable){
        globalTransformableNode = new TransformableNode(arFragment.getTransformationSystem());
        globalTransformableNode.setParent(anchorNode);

        globalTransformableNode.setRenderable(renderable);

        globalTransformableNode.select();
        return globalTransformableNode;
    }

    private Node addRenderableToSceneNoMove(AnchorNode anchorNode, Renderable renderable){
        globalNode = new Node();
        globalNode.setParent(anchorNode);
        globalNode.setRenderable(renderable);
        return globalNode;
    }


    //Dialog Fragment Methods
    private void onUsernameOkPressed(String dialogValue){
        userName = dialogValue;
    }
    private void onPasswordOkPressed(String dialogValue){
        password = dialogValue;
    }

    //Button Press Methods
    private void onConfirmPressed(){
        try{
            Cursor userCursor;
            db.open();
            userCursor = db.getUserByUsername(userName);
            if(userCursor.moveToFirst()){
                if(userCursor.getString(1).equals(userName) && userCursor.getString(2).equals(password)){
                    globalTransformableNode.setRenderable(null);
                    addRenderableToScene(globalAnchorNode, walletRenderable);
                    getTransactions();
                    chequingAmount = userCursor.getString(3);
                    savingAmount = userCursor.getString(4);
                    hasLoggedIn = true;
                    buildAccountWindow();
                    Toast.makeText(this, "Here is your wallet, tap a surface to view your account", Toast.LENGTH_LONG).show();
                }
                else{
                    Toast.makeText(this, "Incorrect Password", Toast.LENGTH_LONG).show();
                }
            }
            else{
                Toast.makeText(this, "No users found with that username", Toast.LENGTH_LONG).show();
            }
            db.close();
        }
        catch(Exception ex){
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
            Log.d("SQL Error", ex.getMessage());
        }



    }
    private void cancelPressed(){
        Collection<Anchor> allAnchors = arFragment.getArSceneView().getSession().getAllAnchors();
        for(Iterator<Anchor> iter = allAnchors.iterator(); iter.hasNext();){
            Anchor anchor = iter.next();
            anchor.detach();
        }
    }

    //Utility Methods
    public void populateTransactions(){
        transactions.add(new Transaction("Deposit", "13.89", "Savings", "tdchallenge"));
        transactions.add(new Transaction("Deposit", "120.45", "Savings", "tdchallenge"));
        transactions.add(new Transaction("Deposit", "20.00", "Savings", "tdchallenge"));
        transactions.add(new Transaction("Deposit", "60.00", "Savings", "tdchallenge"));
        transactions.add(new Transaction("Deposit", "25.00", "Savings", "tdchallenge"));
        transactions.add(new Transaction("Deposit", "13.89", "Savings", "tdchallenge"));
        transactions.add(new Transaction("Deposit", "1000.00", "Savings", "tdchallenge"));
        transactions.add(new Transaction("Deposit", "614.22", "Savings", "tdchallenge"));

        transactions.add(new Transaction("Deposit", "400.00", "Chequing", "tdchallenge"));
        transactions.add(new Transaction("Withdraw", "150.89", "Chequing", "tdchallenge"));
        transactions.add(new Transaction("Deposit", "200.00", "Chequing", "tdchallenge"));
        transactions.add(new Transaction("Withdraw", "250.34", "Chequing", "tdchallenge"));
        transactions.add(new Transaction("Deposit", "55.79", "Chequing", "tdchallenge"));
    }

}
package tdapp.paytonaidan.com.tdchallengeapp;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
/**
 * Created by lwong on 9/28/2015.
 */
public class DBAdapterForUserDB {
    static final String KEY_ROWID = "_id";
    static final String KEY_NAME = "name";
    static final String KEY_PASSWORD = "password";
    static final String KEY_CHEQUING_AMT = "chequing";
    static final String KEY_SAVING_AMT = "saving";
    static final String TAG = "DBAdapter";

    static final String DATABASE_NAME = "UsersDB";
    static final String DATABASE_TABLE = "UserTable";
    static final int DATABASE_VERSION = 2;

    static final String DATABASE_CREATE =
            "create table UserTable (_id integer primary key autoincrement, "
                    + "name text not null, password text not null, chequing text, saving text);";

    final Context context;

    DatabaseHelper DBHelper;
    SQLiteDatabase db;

    public DBAdapterForUserDB(Context ctx)
    {
        this.context = ctx;
        DBHelper = new DatabaseHelper(context);
    }

    private static class DatabaseHelper extends SQLiteOpenHelper
    {
        DatabaseHelper(Context context)
        {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db)
        {
            try {
                db.execSQL(DATABASE_CREATE);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
        {
            Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS UserTable");
            onCreate(db);
        }
    }

    //---opens the database---
    public DBAdapterForUserDB open() throws SQLException
    {
        db = DBHelper.getWritableDatabase();
        return this;
    }

    //---closes the database---
    public void close()
    {
        DBHelper.close();
    }

    //---insert a contact into the database---
    public long insertUser(String name, String password, String chequing, String saving)
    {
        ContentValues initialValues = new ContentValues();
        initialValues.put(KEY_NAME, name);
        initialValues.put(KEY_PASSWORD, password);
        initialValues.put(KEY_CHEQUING_AMT, chequing);
        initialValues.put(KEY_SAVING_AMT, saving);
        return db.insert(DATABASE_TABLE, null, initialValues);
    }

    //---deletes a particular contact---
    public boolean deleteUser(long rowId)
    {
        return db.delete(DATABASE_TABLE, KEY_ROWID + "=" + rowId, null) > 0;
    }

    //---retrieves all the contacts---
    public Cursor getAllUsers()
    {
        return db.query(DATABASE_TABLE, new String[] {KEY_ROWID, KEY_NAME,
                KEY_PASSWORD, KEY_CHEQUING_AMT, KEY_SAVING_AMT}, null, null, null, null, null);
    }

    //---retrieves a particular contact---
    public Cursor getUser(long rowId) throws SQLException
    {
        Cursor mCursor =
                db.query(true, DATABASE_TABLE, new String[] {KEY_ROWID,
                                KEY_NAME, KEY_PASSWORD,KEY_CHEQUING_AMT, KEY_SAVING_AMT}, KEY_ROWID + "=" + rowId, null,
                        null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    public Cursor getUserByUsername(String username) throws SQLException{
        Cursor mCursor =
                db.query(true, DATABASE_TABLE, new String[] {KEY_ROWID, KEY_NAME, KEY_PASSWORD, KEY_CHEQUING_AMT, KEY_SAVING_AMT}, KEY_NAME + "='" + username + "'", null,
                        null, null, null, null);
        if(mCursor != null){
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    //---updates a contact---
    public boolean updateUser(long rowId, String name, String password, String chequing, String saving)
    {
        ContentValues args = new ContentValues();
        args.put(KEY_NAME, name);
        args.put(KEY_PASSWORD, password);
        args.put(KEY_CHEQUING_AMT, chequing);
        args.put(KEY_SAVING_AMT, saving);
        return db.update(DATABASE_TABLE, args, KEY_ROWID + "=" + rowId, null) > 0;
    }

}
package tdapp.paytonaidan.com.tdchallengeapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by lwong on 9/28/2015.
 */
public class DBAdapterForTransactionDB {
    static final String KEY_ROWID = "_id";
    static final String KEY_TYPE = "type";
    static final String KEY_AMOUNT = "amount";
    static final String KEY_ACCOUNT = "account";
    static final String KEY_USERNAME = "username";
    static final String TAG = "DBAdapter";

    static final String DATABASE_NAME = "TransactionDB";
    static final String DATABASE_TABLE = "TransactionTable";
    static final int DATABASE_VERSION = 2;

    static final String DATABASE_CREATE =
            "create table TransactionTable (_id integer primary key autoincrement, "
                    + "type text not null, amount text not null, account text not null, username text not null);";

    final Context context;

    DatabaseHelper DBHelper;
    SQLiteDatabase db;

    public DBAdapterForTransactionDB(Context ctx)
    {
        this.context = ctx;
        DBHelper = new DatabaseHelper(context);
    }

    private static class DatabaseHelper extends SQLiteOpenHelper
    {
        DatabaseHelper(Context context)
        {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db)
        {
            try {
                db.execSQL(DATABASE_CREATE);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
        {
            Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS TransactionTable");
            onCreate(db);
        }
    }

    //---opens the database---
    public DBAdapterForTransactionDB open() throws SQLException
    {
        db = DBHelper.getWritableDatabase();
        return this;
    }

    //---closes the database---
    public void close()
    {
        DBHelper.close();
    }

    //---insert a contact into the database---
    public long insertTranscation(String type, String amount, String account, String username)
    {
        ContentValues initialValues = new ContentValues();
        initialValues.put(KEY_TYPE, type);
        initialValues.put(KEY_AMOUNT, amount);
        initialValues.put(KEY_ACCOUNT, account);
        initialValues.put(KEY_USERNAME, username);
        return db.insert(DATABASE_TABLE, null, initialValues);
    }

    //---deletes a particular contact---
    public boolean deleteTransaction(long rowId)
    {
        return db.delete(DATABASE_TABLE, KEY_ROWID + "=" + rowId, null) > 0;
    }

    //---retrieves all the contacts---
    public Cursor getAllTransactions()
    {
        return db.query(DATABASE_TABLE, new String[] {KEY_ROWID, KEY_TYPE,
                KEY_AMOUNT, KEY_ACCOUNT, KEY_USERNAME}, null, null, null, null, null);
    }

    //---retrieves a particular contact---
    public Cursor getTransaction(long rowId) throws SQLException
    {
        Cursor mCursor =
                db.query(true, DATABASE_TABLE, new String[] {KEY_ROWID,
                                KEY_TYPE, KEY_ACCOUNT, KEY_AMOUNT, KEY_USERNAME}, KEY_ROWID + "=" + rowId, null,
                        null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    public Cursor getTransactionsByUsername(String username) throws SQLException{
        Cursor mCursor =
                db.query(false, DATABASE_TABLE, new String[] {KEY_ROWID, KEY_TYPE, KEY_AMOUNT, KEY_ACCOUNT, KEY_USERNAME}, KEY_USERNAME + "='" + username + "'", null,
                        null, null, null, null);
        if(mCursor != null){
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    //---updates a contact---
    public boolean updateTransaction(long rowId, String type, String account, String amount, String username)
    {
        ContentValues args = new ContentValues();
        args.put(KEY_TYPE, type);
        args.put(KEY_ACCOUNT, account);
        args.put(KEY_AMOUNT, amount);
        args.put(KEY_USERNAME, username);
        return db.update(DATABASE_TABLE, args, KEY_ROWID + "=" + rowId, null) > 0;
    }

}
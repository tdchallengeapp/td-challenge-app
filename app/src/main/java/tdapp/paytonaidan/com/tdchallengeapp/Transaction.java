package tdapp.paytonaidan.com.tdchallengeapp;

public class Transaction {
    String type; //withdraw or deposit
    String amount;
    String account;
    String username;

    public Transaction(String type, String amount, String account, String username) {
        this.type = type;
        this.amount = amount;
        this.account = account;
        this.username = username;
    }
}

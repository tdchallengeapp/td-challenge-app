package tdapp.paytonaidan.com.tdchallengeapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.InputType;
import android.text.InputFilter;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

public class UsernameDialog extends DialogFragment{
    interface OkListener{
        void onOkPressed(String dialogValue);
    }

    private OkListener okListener;
    private EditText userName;

    void setOkListener(OkListener okListener){this.okListener = okListener;}

    private LinearLayout getDialogLayout(){
        Context context = getContext();
        LinearLayout layout = new LinearLayout(context);
        userName = new EditText(context);
        userName.setInputType(InputType.TYPE_CLASS_TEXT);
        userName.setLayoutParams(
                new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        userName.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});
        layout.addView(userName);
        layout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        return layout;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder
                .setView(getDialogLayout())
                .setTitle("Enter Username")
                .setPositiveButton(
                        "OK",
                        (dialog, which) -> {
                            Editable userNameText = userName.getText();
                            if (okListener != null && userNameText != null && userNameText.length() > 0) {
                                // Invoke the callback with the current checked item.
                                okListener.onOkPressed(userNameText.toString());
                            }
                        })
                .setNegativeButton("Cancel", (dialog, which) -> {});
        Dialog d = builder.create();
        d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return d;
    }
}

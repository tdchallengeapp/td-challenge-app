package tdapp.paytonaidan.com.tdchallengeapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

public class PasswordDialog extends DialogFragment{
    interface OkListener{
        void onOkPressed(String dialogValue);
    }

    private OkListener okListener;
    private EditText password;

    void setOkListener(OkListener okListener){this.okListener = okListener;}

    private LinearLayout getDialogLayout(){
        Context context = getContext();
        LinearLayout layout = new LinearLayout(context);
        password = new EditText(context);
        password.setTransformationMethod(PasswordTransformationMethod.getInstance());
        //password.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
        password.setLayoutParams(
                new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        password.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});
        layout.addView(password);
        layout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        return layout;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder
                .setView(getDialogLayout())
                .setTitle("Enter Password")
                .setPositiveButton(
                        "OK",
                        (dialog, which) -> {
                            Editable passwordText = password.getText();
                            if (okListener != null && passwordText != null && passwordText.length() > 0) {
                                // Invoke the callback with the current checked item.
                                okListener.onOkPressed(passwordText.toString());
                            }
                        })
                .setNegativeButton("Cancel", (dialog, which) -> {});
        Dialog d = builder.create();
        d.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return d;
    }
}
